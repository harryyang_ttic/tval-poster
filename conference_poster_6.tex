%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% a0poster Portrait Poster
% LaTeX Template
% Version 1.0 (22/06/13)
%
% The a0poster class was created by:
% Gerlinde Kettl and Matthias Weiser (tex@kettl.de)
% 
% This template has been downloaded from:
% http://www.LaTeXTemplates.com
%
% License:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

%\documentclass[28pt,portrait]{a0poster}
\documentclass[a0,portrait]{a0poster}

\usepackage{multicol} % This is so we can have multiple columns of text side-by-side
\columnsep=100pt % This is the amount of white space between the columns in the poster
\columnseprule=3pt % This is the thickness of the black line between the columns in the poster

\usepackage[svgnames]{xcolor} % Specify colors by their 'svgnames', for a full list of all colors available see here: http://www.latextemplates.com/svgnames-colors

\usepackage{times} % Use the times font
%\usepackage{palatino} % Uncomment to use the Palatino font

\usepackage{graphicx} % Required for including images
\graphicspath{{figures/}} % Location of the graphics files
\usepackage{booktabs} % Top and bottom rules for table
\usepackage[font=small,labelfont=bf]{caption} % Required for specifying captions to tables and figures
\usepackage{amsfonts, amsmath, amsthm, amssymb} % For math fonts, symbols and environments
\usepackage{wrapfig} % Allows wrapping text around tables and figures
\usepackage{array}
\usepackage{float}
\usepackage[framemethod=TikZ]{mdframed}
\DeclareMathOperator*{\argmin}{arg\,min}
\begin{document}
\LARGE
%----------------------------------------------------------------------------------------
%	POSTER HEADER 
%----------------------------------------------------------------------------------------

% The header is divided into two boxes:
% The first is 75% wide and houses the title, subtitle, names, university/organization and contact information
% The second is 25% wide and houses a logo for your university/organization or a photo of you
% The widths of these boxes can be easily edited to accommodate your content as you see fit

\begin{minipage}[b]{0.9\linewidth}
\veryHuge \color{NavyBlue} \textbf{Sensor-Prediction Evaluation of Stereo Algorithms
} \color{Black}\\\\ % Title
\huge \textbf{Harry Yang \quad\quad\quad David McAllester}\\[0.5cm] % Author(s)
\huge Toyota Technological Institute at Chicago\\[0.4cm] % University/organization
\Large \texttt{harryyang@ttic.edu \quad mcallester@ttic.edu}\\
\end{minipage}
%
\begin{minipage}[b]{0.25\linewidth}
\includegraphics[width=10cm]{logo.jpg}\\
\end{minipage}

%\vspace{1cm} % A bit of extra whitespace between the header and poster content

%----------------------------------------------------------------------------------------

\begin{multicols}{2} % This is how many columns your poster will be broken into, a portrait poster is generally split into 2 columns

%----------------------------------------------------------------------------------------
%	ABSTRACT
%----------------------------------------------------------------------------------------

\begin{mdframed}[roundcorner=15pt, frametitle
={Abstract},frametitlerule=true,frametitlebackgroundcolor=brown!30!white,]
  We consider the problem of quantitative evaluation of pixel-level vision systems on images in the wild. For stereo depth estimation, image segmentation, and particularly for scene flow, accurate ground truth can be difficult to obtain. We present a method for quantitative evaluation of pixel-level vision algorithms based directly on lidar data and without any pixel-level ground truth labeling. We note that this evaluation methodology opens the practical possibility of autonomous learning of vision algorithms — robotic learning from simply being in the world.
\end{mdframed}

%----------------------------------------------------------------------------------------
%	INTRODUCTION
%----------------------------------------------------------------------------------------
\begin{center}
\includegraphics[width=0.1\linewidth]{figures/arrow.jpeg}
\end{center}
%\hfill \break
\begin{mdframed}[roundcorner=15pt, frametitle
={Introduction},frametitlerule=true,frametitlebackgroundcolor=brown!30!white,]
\begin{itemize}
\item \textbf{Motivation:} The difficulty of in-the-wild evaluation related to constructing the ground truth. 
\item \textbf{Example:} KITTI is limited to static scenes, and their ground truth labelling is subject to calibration error.
\item \textbf{Solution:} We propose to evaluate based directly on lidar data and without any pixel-level ground truth labelling, and let calibration be part of the inference and training process.
\item \textbf{Contribution:} The relationship between sensor prediction evaluation and autonomous learning is established.
\end{itemize}
 
\begin{center}
\includegraphics[width=0.8\linewidth]{figures/outlier2.png}
\captionof{figure}{\color{Green} KITTI disparity error}
\end{center}

\end{mdframed}


%----------------------------------------------------------------------------------------
%	OBJECTIVES
%----------------------------------------------------------------------------------------
\begin{center}
\includegraphics[width=0.1\linewidth]{figures/arrow.jpeg}
\end{center}

\begin{mdframed}[roundcorner=15pt, frametitle
={Evaluation},frametitlerule=true,frametitlebackgroundcolor=brown!30!white,]
For each lidar pulse, the criteria for correct prediction is defined as: 
\[
|Z-Z'|<\frac{3Z^2}{{f_x}_0 B_0}.
\]
Here $Z$ and $Z'$ are the Z-coordinate of lidar point and user prediction in native lidar coordinates. ${f_x}_0$ and $B_0$ are the default focal length and baseline value. This criteria is an approximation 3-pixel error in disparity map evaluation used in KITTI.\\
\noindent\textbf{Occluded point separation.} Similar to KITTI, we report the error rate on both the entire points and non-occluded points. We use an automatic raycasting approach to separate occluded points from other points. 
\end{mdframed}



\begin{mdframed}[roundcorner=15pt, frametitle
={Converters and Calibration},frametitlerule=true,frametitlebackgroundcolor=brown!30!white,]
We provide two different converters able to convert a disparity input to lidar prediction: the default slanted plane converter and the raw disparity converter. The first one uses the slanted plane model to generate a set of 3D planes, and the latter one directly generates 3D point cloud from disparity map (Fig.~\ref{fig:3D_planes} top).\\
After acquiring the set of 3D planes or points, we propose an \textit{rasterization} approach to predict the lidar flight time: we walk along each lidar ray until it hits a plane or goes behind a lidar point. The time a ray takes to travel is taken as the its flight time predicted. We also introduce an automatic scheme to detect occlusion situation occurring due to displacement of camera and lidar (Fig.~\ref{fig:3D_planes} bottom).
\begin{center}
\includegraphics[width=0.55\linewidth]{figures/000254_scene.png}
\includegraphics[width=0.6\linewidth]{figures/occlusion3.png}
\captionof{figure}{\color{Green} Top: 3D planes converted from slanted plane smoothing. Bottom: occlusion handling. $P$ is the actual lidar position visible to lidar, but the ray misses the corresponding plane as part of it is occluded by the plane in the front.}
\label{fig:3D_planes}
\end{center}
We propose an automatic calibration approach that seeks the optimal transform between camera and lidar. The transform is assumed to minimize the error of SPS-ST~\cite{yamaguchi2014efficient}, one of the learning stereo algorithms on KITTI leaderboard to date.
\end{mdframed}

\begin{center}
\includegraphics[width=0.05\linewidth]{figures/arrow.jpeg}
\end{center}

\begin{mdframed}[roundcorner=15pt, frametitle
={Data Collection and Results},frametitlerule=true,frametitlebackgroundcolor=brown!30!white,]
Our evaluation benchmark contains a training set, a development set and a test set, each of which contains 291 image pairs and lidar scans. The scene flow dataset contains 35 independent video sequences. We ran state-of-art stereo methods on our benchmark, and their scores comparing with KITTI are:

\begin{center}
\Large
\begin{tabular}{r c c c c}
\toprule
\textbf{Stereo} & \textbf{Non-Occ-SPC} & \textbf{All-SPC} & \textbf{All-RDC} & \textbf{KITTI-All}\\
\midrule
 SPS-St & 4.55\% & 5.79\% & 8.41\% & 4.41\% \\
 rSGM &5.84\%& 7.33\% & 7.15\% & 6.60\%\\
 opencv-BM &6.61\%& 7.88\% & 13.12\% & 13.76\% \\
 opencv-SGBM &9.54\%& 10.32\% & 12.52\% & 9.13\%\\
 ELAS &10.80\%& 11.09\% & 11.01\% & 9.96\%\\ 
\bottomrule
\end{tabular}
\captionof{table}{\color{Green} Performance of different stereo algorithms. SPC and RDC refer to slanted plane converter and raw disparity converter respectively.}
%\end{wraptable}
\end{center}
\end{mdframed}

\normalsize
\nocite{*} % Print all references regardless of whether they were cited in the poster or not
\bibliographystyle{plain} % Plain referencing style
\bibliography{sample} % Use the example bibliography file sample.bib

%----------------------------------------------------------------------------------------
%	ACKNOWLEDGEMENTS
%----------------------------------------------------------------------------------------


%----------------------------------------------------------------------------------------

\end{multicols}
\end{document}